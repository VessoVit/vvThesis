#Background

The processes taking place in this project are going to be guided by "Creative Technology Design Processes", as A.Mader and W.Eggink are presenting in their paper. [1] In the paper the authors suggest Spiral Models, which similar to Agile Development allows looping through different stages / sub-stages, making the processes much more flexible for both research and development.

The project is presenting two stages of development: Visualisation and Interactive Experience for (multi)user environment. In parallel a research on the topics of BCI, Generative Art and User behaviour is taking place. 





---

[1] Mader, Angelika and Eggink, Wouter (2014) A design process for creative technology. In: 16th international conference on engineering & product design education "Design education & human technology relations", Enschede, 4-5 September 2014, 4-5 September 2014, Enschede.