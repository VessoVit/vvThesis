# Prior art
In the following chapter different prior art towards visualisations will be outlined.

__Meta-Mind__, Berlin 2014 || [Homepage](http://www.meta-mind.de) || [Video](http://video.metavolution.de/)  
[MetaVolution](https://github.com/metavolution) is German hacker and self-quantified and one of the person behind [ Open Brain Hacking](https://github.com/openbrainhacking) group. Big part of their work is related to this project, with both relevance in visuals and BCI. The results of their effort can be seen in the video below.  [1:37](https://youtu.be/q_2rhnZ1PW8?t=1m37s)  EEG raw data, [2:40](https://youtu.be/q_2rhnZ1PW8?t=2m34s) Music Beats out of Brain States + Visuals, [6:30](https://youtu.be/q_2rhnZ1PW8?t=6m30s) Hardware visualisation, [8:10](https://youtu.be/q_2rhnZ1PW8?t=8m5s) music hardware application.  [8:40](https://youtu.be/q_2rhnZ1PW8?t=8m39s) and [09:50](https://youtu.be/q_2rhnZ1PW8?t=9m50s) manipulation of liquids.  (..)  

<iframe width="640" height="315" src="https://www.youtube.com/embed/q_2rhnZ1PW8" frameborder="0" allowfullscreen></iframe>  
 
__ofxEmotiv*__ || [Project Page](http://thisisnotanumber.org/proyecto.php?prid=5)   
The project seem rather created as result of one-time experiment, as has been update for a long time. At its current form, it works under windows with limited build. However the projects provides a good example of integrating openFrameworks with the native C++ SDK of Emotiv. 

![ofxEmotiv](http://thisisnotanumber.org/img/ofxemotiv_2.jpg "OfxEmotiv")  

__Visualizing Heartbeat__ || [Project Page](http://www.phanv.com/blog/visualizing-heartbeat/ )
The project by Pan is inspired by organic and real life rhythms to drive natural animation. For the purpose of heartbeat visualisation two factors are taken into account: 1) Pulse rate for the speed of the animation and 2) the amplitude for the brightness of the visualisation.
<iframe src="https://player.vimeo.com/video/41274910" width="640" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>   


 __Databetes Data Visualization__ - [Project Page](https://dougkanter.wordpress.com/2011/12/09/7729-icm-final-project-databetes-data-visualization/)  
 As part of ICM Final Project in 2011, the artist Doug Kanter created a simple visualisation for blood sugar readings.   
<!-- ![Blood Sugar Readings]( "Blood Sugar Visualisation") -->

<img src="https://dougkanter.files.wordpress.com/2011/12/month1.jpg" width="500" height="500">

(...)

---

Emotiv store is also offering few commercial visualisations for brain state. Those are very technical aiming torwards scientific audience. (..)