#Bibliography
--- 

##Visual:

The Role of Aesthetics in Engineering . Prof . Rolf A , Faste Director , Product Design Department of Mechanical Engineering Stanford University Japan Society of Mechanical Engineers ( JSME ) Journal. (1995).

Galanter, P. (n.d.). What is Generative Art? Complexity Theory as a Context for Art Theory.

Dorin, A., Mccabe, J., Mccormack, J., Monro, G., & Whitelaw, M. (2012). A framework for understanding generative art. doi:10.1080/14626268.2012.709940

Whitelaw, M. (n.d.). System Stories and Model Worlds: A Critical approach to generative art.

The Aesthetics of Graph Visualisation. (2007). Computational Aesthetics in Graphics, Visualization, and Imaging, pp. 1–8.

Alan Dorin, Jonathan Mccabe, Jon Mccormack, Gordon Monro, andMitchell Whitelaw. A framework for understanding generative art. 2012.  

Massimo Franceschet. Complex Beauty. pages 1{12, 2014.  


---

## BCI:

Numbers from the Heart - Professor records brainwaves while meditating http://quantifiedself.com/2010/04/numbers-from-the-heart/

A.Rosa D. Migotina, C. Isidro.  Brain Art :  Abstract Visualization ofSleeping Brain. pages 141{153, 2011.

<!-- DIY EEG ( and ECG ) Circuit 
[URL](https://www.evernote.com/shard/s44/sh/ffd01b94-f402-4454-89b2-8b2cd2c0047e/d10dc5242d3d0d67)  -->

---

## Books:

Scott E. Page, Diversity and Complexity, 2011. 

Douglas R. Hofstadter, "Gödel, Escher, Bach: an Eternal Golden Braid", 1999

    * The Code of Modern Art, p.703-704 
    * From Computer Haiku to an RTN-Grammar, p.619-620 on computer generated artistic creation

---

### Reading list:

[2] Val Tsourikov. Architecture of Self Learning A . I . Platform for GenerativeArt and Films. pages 254{260, 2012.  
[4] Nijholt A.  BCI for games: a 'state of the art' survey.EntertainmentComputing - ICEC 2008, (2):225{228, 2009.  
[6] Matilde Marcolli. Entropy and Art the view beyond Arnheim. 2015.  
[7] Q B Zhao, L Q Zhang, and J Li. Multi-Task BCI for Online Game Control.Autonomous Systems - Self-Organization, Management, and Control, pages29{37183, 2008.  
[8] a Dorin.  Physicality and Notation, Fundamental Aspects of GenerativeProcesses in the Electronic Arts.Proceedings of First Iteration, pages pp80{91, 1999.  
[9] Hayrettin Gurkok, Bram Van De Laar, Danny Plass Oude Bos, MannesPoel, and Anton Nijholt. Players' opinions on control and playability of aBCI game.Lecture Notes in Computer Science (including subseries LectureNotes in Articial Intelligence and Lecture Notes in Bioinformatics), 8514LNCS(PART 2):549{560, 2014.  
[10] Evangelia Balanou, Mark Van Gils, and Toni Vanhala.  State-of-the-artof wearable EEG for personalized health applications.Studies  in  HealthTechnology and Informatics, 189:119{124, 2013.  
[11] Mitchell Whitelaw. System Stories and Model Worlds: A Critical approachto generative art.]