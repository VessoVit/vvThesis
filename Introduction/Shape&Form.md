# Shape and Form

##The Grid
    ...indexation system that goes from the most basic regular and uniform geometrical tilings to the most complex and irregular ones; a growing array of shapes, vertex transitivity, and symmetry axis present us with an index of plane subdivision possibilities [Nicolai 2009]
[Nicolai 2009] The Grid, http://shop.gestalten.com/grid-index.html, accessed April 2015

##Gestalt psychology

    Gestalt psychologists working primarily in the 1930s and 1940s raised many of the research questions that are studied by vision scientists today.
    ---
    The Gestalt Laws of Organization have guided the study of how people perceive visual components as organized patterns or wholes, instead of many different parts. Gestalt is the German word "Gestalt" that partially translates to "configuration or pattern" along with "whole or emergent structure". According to this theory, there are six main factors that determine how the visual system automatically groups elements into patterns: Proximity, Similarity, Closure, Symmetry, Common Fate (i.e. common motion), and Continuity.

[Gestalt Psychology - Wikipedia](http://en.wikipedia.org/wiki/Gestalt_psychology)