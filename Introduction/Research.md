# First Chapter

## Research 

### Related Work

Meta-Mind, Berlin 2014 || [Homepage](http://www.meta-mind.de) || [Video](http://video.metavolution.de/)   
<iframe width="560" height="315" src="https://www.youtube.com/embed/q_2rhnZ1PW8" frameborder="0" allowfullscreen></iframe>

ofxEmotiv* || [Project Page](http://thisisnotanumber.org/proyecto.php?prid=5)  
![ofxEmotiv](http://thisisnotanumber.org/img/ofxemotiv_2.jpg "OfxEmotiv")  
    * Not competable with the latest version of OpenFrameworks, however good visuals.

Visualizing Heartbeat - http://www.phanv.com/blog/visualizing-heartbeat/  
<iframe src="https://player.vimeo.com/video/41274910" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>   


ICM Final Project / Databetes Data Visualization - https://dougkanter.wordpress.com/2011/12/09/7729-icm-final-project-databetes-data-visualization/
<!-- ![Blood Sugar Readings]( "Blood Sugar Visualisation") -->

<img src="https://dougkanter.files.wordpress.com/2011/12/month1.jpg" width="500" height="500">

    blood sugar readings
    
#### Sets

Emotiv EPOC, Emotiv Insights


#### Applications

Brain https://github.com/kitschpatrol/Brain

EGG Arduino Portable EEG http://www.instructables.com/id/Mini-Arduino-Portable-EEG-Brain-Wave-Monitor-/

Arduino Brain-Wave http://www.instructables.com/id/Arduino-brain-wave-reader/