# Introduction

In today world of rapidly-advancing technology and ever shortening innovation cycles we observe dynamics of new exciting technology. Out of necessity or just for the science of it, new input methods for interaction with computers/machines are being accessible to the general public ever more often. An important player in the world of new input devices is Brain Computer Interaction and the neurological headsets available.

From the computer mouse and keyboard (...)


# Planning 

| Design and Development     | Research                                  |
|----------------------------|-------------------------------------------|
| BCI input                  | Existing Applications                     |
| Interpreting signals       | Traditional Creativity                    |
|                            | Generative Art                            |
| Methods for visualisation  | BCI (Effective, Cognitive and Expressive) |
|                            |                                           |
| Game with visual input     | Aesthetics, fun,factor                    |
|                            | Behavioural goals                         |
| Virtual Environment        |                                           |
| Multilayer experience     | Cooperation vs Competition                |