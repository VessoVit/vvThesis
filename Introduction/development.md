# Development

In the beginning of the creation stage of this project, a number of "hacks and explorations" were performed. Those are aiming to give an good overview of the performance and the limits of different methods, techniques and technology in relevance to the task given.

##Visuals 
The first hack and exploration addressed drawing of vectors, openGL graphics and shaders (GLSH).

The results can be found in section [3.1] Hacks and Exploration.
<!-- * [dev-visual-#1](output/imagesOutput.md) -->

<!-- ##Neuroheadset -->

###Emotiv 

The emotiv headset has a rather rich development resource base. Available source codes in Java, C++, C# and Objective-C opens the opportunity for development on multiple platforms, including creative coding frameworks as Processing and openFrameworks. 

The SDK provides access to EmoEngine, which has readings and detection in three suits: cognitive, affective and expressive. For the focus of this project we will look further into affective, which provides __Valence, Engagement/Boredom, Meditation and Frustration__ scores. [1]

[1] Emotiv SDK for .Net, http://emotiv.com/api/windows/epoc/DotNetEpocSDK_APIREF/class_emotiv_1_1_emo_state.html , accessed: April 2015

###openFrameworks and emotiv 
openFrameworks is C++, creative coding framework. It does provide wide range of (graphical) functions. Similar to processing, the library gives creative freedom to do (mathematicly) interesting results. oF is suppoorting much better shaders and 3D representation, hance it is more desirable choice for final product. 

While there is ofxEmotiv plugin, this one is not official and vaglu supported. However, the Emotiv's EDK offers C++ library and can be work with directly. [..]

###Unity3D Game Engine
At this current stage, the Emotiv Unity3D plugin is available for sell, however it does not include multiplatform support, working under windows only. [..]

###Mind Your OSCs
OSC (open-source Control) is protocol for netowking for music, computer and multimedia, bring electronic music instruments into digital (synced) world. The technology is commonly used for other purposes as well, such as Emotiv. For the purpose of BCI, a package called MindYourOSCs is available. 

This method allows is to alternatively connect to processing (e.g.), by accessing the SDK the program.

###OpenVibe [To be tested]

The platform OpenVibe is dedicated to design and testing of BCI it has a wide range of features, such as "Multiplatform, graphic language and powerfull signal processing".

