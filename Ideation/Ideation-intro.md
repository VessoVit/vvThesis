#Ideation

The starting point for the Creative process of this project is centre around the clients needs, Human-Media-Interaction group @ UT and Technology aspect of the project. BCI is represented by dozen commercial products and limited capabilities related to methods of contact and price. Emotiv's EPOC is going to be used for the testing of this project and hence its technical specification, 'easy-to-implement' and quality of Documentation & SDK are going to be define moments for this project.

The process of ideation further continued with brainstorming and tinkering in direction Art, Aesthetics and elements of a good artistic art-work. Further investigation in Generative Art took place. As suggested in the Mader[1] for Ideation of this project a loop between Creative idea and technology allowed 'hacks and explorations' as a base of this processes. 

Those were required to define the scope of the project and the limitation/borders of the technology, computing capabilities of the available hardware and behaviour of the BCI headset in scenarios. [...]

 