## Research Statement

Given the current state of EEG applications, can we develop unifying visualisations representing brain states for entertainment?

__Sub-questions:__

 1. What is the current “State of the Art” in BCI applications development?
 2. Which aesthetics has most effect on helping users understanding their part in BCI?
 3. Can we visualize mental states based on brainwaves and conscious thoughts in comprehensive manner?
 4. Can we appropriate those visualizations for enjoyable game experience? 
