##Brainstorming

The first set of requirements was used for a starting point of the idea generation processes. To those a set of technical aspects were taken into consideration. Reference to those can be found in Appendix.

The first brainstorm was focused on mapping high level requirements for development of the visualisation and the interactive experience. As result a further focus on Methods of Input/Output, Methods for Visualisation and Game objective was outlined for further investigation.

Second brainstorm session take place couple of weeks later, after preliminary research on different methods, approaches and Creativity in context of computer generated graphics. The result of this brainstorming lists, which aims to address the project in a modular approach. [TODO:REF] Further challenges of the projects were identified and noted.

The last brainstorming session of the first stage of the project, resulted into a system design and duo-list of the parallel Design&Development / Research. 

---

##Tinkering 

As part of the tinkering processes, Hacks and Explorations were done in order to see the maximum of those involved explorations on shapes (vectors, lines and grids), forms generated in 2D drawn in depth which brings the illusion of 3D and 3D (vertexes, camera, environment). Those bring the building blocks for generative art.

Further hacks and explorations were done in connecting those initial sketches with EPOC. Here some exploration were into EPOC's different language libraries and their comparability with technology platforms of interest, such as: C#, Mono / Unity3D, C++ Linux and Mac implementations and OSC, network based communication. 



