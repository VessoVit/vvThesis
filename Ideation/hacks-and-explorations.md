Curves:
![Visual 7](../output/img/vis-7.png)
![Visual 8](../output/img/vis-8.png)

Colours:
![Visual 10](../output/img/vis-10.png)
![Visual 11](../output/img/viz-11.png)
![Visual 12](../output/img/viz-12.png)


Irregular Pattern Grid:
![Irregular Pattern](../images/grid-irregular-pattern.png)

Triangulated vertexes
![Visual 3](../output/img/vis-3.png)
![Visual 4](../output/img/vis-4.png)
![Visual 5](../output/img/vis-5.png)
![Visual 6](../output/img/vis-6.png)

Complex image:
![Visual 1](../output/img/vis-1.png)
![Visual 2](../output/img/vis-2.png)
![Visual 2](../output/img/vis-9.png)

Draw 1px line for Frustration, Excitement and Long term excitement 
![Alt text](../output/img/br-paint.png)
![Alt text](../output/img/br-paint-2.png)

Order vs Disorder: Excitement vs Frustration 
![Alt text](../output/img/br-shape-1.png)
![Alt text](../output/img/br-shape-2.png)
![Alt text](../output/img/br-shape-3.png)