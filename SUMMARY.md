# Summary

Add your chapters and articles in the following list:

<!-- * [Introduction](Introduction/readme.md) -->
    
* [Background](Introduction/background.md)
    * [Literature](Introduction/Literature.md)
    * [Research statement](Ideation/research-q.md)

    <!-- * [EEG-sets](Introduction/EEG-sets.md) -->
    <!-- * [Snape and Form](Introduction/Shape&Form.md) -->

* [Ideation](Ideation/Ideation-intro.md)
    <!-- * [Hacks and Exploration](Introduction/development.md) -->
    * [Users]()
    * [Creative Idea](Ideation/CreaTe-id.md)
        * [Process](Ideation/brainstorming.md)
        * [Prior-Art](Introduction/prior-art.md) 
        <!-- * [Hacks and Exploration](Ideation/hacks-and-explorations.md) -->

    * [Customer Requirements](link)
* [Specification](link)
    * [Development log](Introduction/development.md) 
* [Realisation](link)
* [Evaluation](link)
* [Conclusion](link)

* [Appendix]()
    * [Bibliography](Introduction/bibliography.md)
    * [Brain Storming Output](appendix/brainstorming.md)
    * [Hacks and Explorations Images](Ideation/hacks-and-explorations.md)
* [Glossary](GLOSSARY.md)