Initial Brainstorming to map project focus.
![Brainstorming](../images/bs-0.png "Optional title")

Project wide brainstorming results.
![Brainstorming](../images/br-1.png "Optional title")

Research focus and simple systems interaction
![Brainstorming](../images/bs-2.png "Optional title")