The HMI group at University of Twente is conducting research on topic of Human Computer Interaction. One of the subjects is Brain Computer Interaction (BCI) and more specific how can BCI improve game experience. The topic has previously been addressed in relation to enhancing existing games experience with BCI. However there is the need for new methods for BCI specific visualisations and game experiences.

Electroencephalography (EEG) headsets are commonly used to deliver brain data. The current research and technology for EEG headsets has advancing in some fields such as medical sector, but lacking development in others such as entertainment. Emotiv for example, does present a consumer ready product, but rather limited applications development for consumers. Regardless that limit, we do observe more and more consumer interest for BCI applications in home and entertainment.

Visual perception is one of the primary human skills and allows us to understand the world, appreciate its beauty and aesthetics. Visualisation plays a major role for constructing aesthetically pleasant results. Those results are important aspect, since they have the most direct relationship to the players experience  and vital for creation of innovative products.  Good visualisation defines the user experience and enhances the underlying mechanics.  


### Challenge	
								
Developing a visualisation for specific brain states is the first challenge of this project. In order to provide pleasant user experience for all users a deeper understanding of user input is required. Factors such as level of stress during the day as well as technical aspects of conductive media as in hair difference can influence the results. Those factors have to be identified and considered in software or hardware.

Further challenge of this project is the game aspect as proof of concept for brain state visualisation(s), including attainment of behavioural goals. Those behavioural goals will include challenges in which the gamer(s) have to reach a specific brain state with the help of related visualisation. One of the design challenges in this research is the design of games in which BCI improves the overall user experience. 

A number of technical challenges are also visible at this stage. First and foremost is obtaining the data and making sense of it. Further, classification and analysis of the raw data in a way that it will be best of use for the project is a challenge with many unknowns. Last, but not least the implementation.

###Question(s) 

#Given the current state of EEG headsets, can we develop unifying visualisations representing brain states for entertainment?

Sub-questions:

#### 1. What is the current “State of the Art” in BCI applications development?
#### 2. Which aesthetics has most effect on helping users understanding their part in BCI?
#### 3. Can we visualize mental states based on brainwaves and conscious thoughts in comprehensive manner?
#### 4. Can we appropriate those visualizations for enjoyable game experience? 


### Practical Aspect(s) 
                            
Working with consumer-ready EEG headsets is enabling options for development of applications. For example Emotiv is having number of patents on making sense of the raw data. Those include technology for recognising 3 mental states recognition (based on brainwaves), 13 conscious thoughts, facial expressions and head movements. All those are accessible for the developers to create applications, giving opportunities for creative solutions.

Technology constrains includes the readiness level of the EEG headsets and their Software-development-kits. Those are crucial, as the time span of the project does not allow much of modifications in that direction. While some open-source alternatives exist and the possibility for exchanging the underling firmware of given set to gain more insights, the process is very time consuming. 

Different age group would appreciate aesthetics differently. This might be a problem for developing an enjoyable game experience. 



