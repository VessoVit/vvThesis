
# Introduction

In today world of rapidly-advancing technology and ever shortening innovation cycles we observe dynamics of applications and methods for human-computer interaction. Out of necessity or just for the science of it, new input methods for interaction with computers/machines are being accessible to the general public ever more often. An important player in the world of new input devices is Brain Computer Interaction and the available neurological headsets.

From the computer mouse and keyboard to BCI. [...]

<!-- This thesis is taking an investigation on existing applications, generative art and visualisations in order to present a product specificity design for BCI input. Initial description of the project can be found as reference downer.  -->


# Planning 

| Design and Development     | Research Topics                           |
|----------------------------|-------------------------------------------|
| BCI input                  | Existing Applications                     |
| Interpreting signals       | Traditional Creativity                    |
|                            | Generative Art                            |
| Methods for visualisation  | BCI (Effective, Cognitive and Expressive) |
|                            |                                           |
| Game with visual input     | Aesthetics, fun,factor                    |
|                            | Behavioural goals                         |
| Virtual Environment        |                                           |
| Multilayer experience     | Cooperation vs Competition                |

![Planning timeline](images/planning-pres.png "Planning Timeline")
