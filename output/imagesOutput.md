Simple bazier curves:
![Visual 7](img/vis-7.png)
![Visual 8](img/vis-8.png)

Triangulated vertexes
![Visual 3](img/vis-3.png)
![Visual 4](img/vis-4.png)
![Visual 5](img/vis-5.png)
![Visual 6](img/vis-6.png)

    Complex image:
![Visual 1](img/vis-1.png)
![Visual 2](img/vis-2.png)


